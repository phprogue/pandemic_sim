import std.stdio;
import std.math;
import std.random;
import ui;

// physical world limits
enum X_LIMIT = 5000;
enum Y_LIMIT = 5000;

// general simulation parameters
enum SIM_HOURS = 5000;
enum NUM_PEOPLE = 1000;

// stages of the disease
enum DISEASE_STATUS { VULNERABLE, INFECTED, IMMUNE, DEAD }

// how many hours the infection lasts
enum INFECTION_TIME = (24 * 20);

// likelihood of infection when exposed
enum INFECTION_PROBABILITY = 0.5;

// disease dynamics
enum NORMAL_FATALITY_RATE = 0.01; // can get medical help
enum SATURATED_FATALITY_RATE = 0.06; // can't get medical help
enum INFECTION_PROXIMITY = 5.0; // how close to transmit disease
enum INITIAL_INFECTIONS = 5;

// how many sick people at a time to saturate the health care system
enum SATURATION_THRESHOLD = (NUM_PEOPLE / 5);

enum NUM_POPULAR_PLACES = 15;
// fraction of people practicing social distancing
enum DISTANCING_PROBABILITY = 0.8;
// home probabilities for those distancing and those not
enum DISTANCING_HOME_PROBABILITY = 0.98;
enum NOT_DISTANCING_HOME_PROBABILITY = 0.02;
// the maximum speed a person can move
enum PP_TOP_SPEED = 20;
// max time a person will stay at one place
enum MAX_STAY = 4;

bool is_saturated = false;
float proximity = 0.05;
int max_infected_at_once = 0;
int num_dead;

Location[15] popular_places;

struct Location {
    float x;
    float y;
}

struct Person {
    Location loc;
    Location home;
    Location waypoint;
    DISEASE_STATUS status;
    int infection_counter;
    float speed;
    int stay;
    float home_probability;

    void init() {
        this.home = get_location();
        this.loc = this.home; //.x = uniform(0, X_LIMIT);
        //this.loc.y = uniform(0, Y_LIMIT);
        this.status = DISEASE_STATUS.VULNERABLE;
        this.infection_counter = INFECTION_TIME;
        this.speed = -1;
        if (try_event(DISTANCING_PROBABILITY)) {
            this.home_probability = DISTANCING_HOME_PROBABILITY;
        } else {
            this.home_probability = NOT_DISTANCING_HOME_PROBABILITY;
        }
    }
}

int[SIM_HOURS] infection_history;
Person[NUM_PEOPLE] people;

void main()
{
    foreach (ref p; people) {
        p.init();
    }

    for (int i = 0; i < INITIAL_INFECTIONS; i++) {
        people[i].status = DISEASE_STATUS.INFECTED;
    }

    // set up destination locations that people can visit
    foreach (ref w; popular_places) {
        w.x = uniform(0, X_LIMIT);
        w.y = uniform(0, Y_LIMIT);
    }

    writeln("Vuln\tInfect\tImmune\tDead(%)");
    init_ui();
}

int run(int elapsed_hours)
{
    // simulation
    // move people
    for (int p = 0; p < NUM_PEOPLE; p++) {
        if (is_alive(people[p])) {
            move(people[p]);
            progress_disease(people[p]);
        }
    }

    // try to infect
    for (int p = 0; p < NUM_PEOPLE; p++) {
        if (is_alive(people[p])) {
            for (int p2 = 0; p2 < NUM_PEOPLE; p2++) {
                if (p != p2 && is_alive(people[p2])) {
                    try_infect(people[p], people[p2]);
                }
            }
        }
    }

    // report status
    int num_infected = 0;
    int num_immune = 0;
    num_dead = 0;
    int num_vulnerable = 0;
    for (int p = 0; p < NUM_PEOPLE; p++) {
        if (!is_alive(people[p])) {
            num_dead++;
        }
        if (people[p].status == DISEASE_STATUS.IMMUNE) {
            num_immune++;
        }
        if (people[p].status == DISEASE_STATUS.INFECTED) {
            num_infected++;
        }
        if (people[p].status == DISEASE_STATUS.VULNERABLE) {
            num_vulnerable++;
        }
    }

    if (num_infected > max_infected_at_once)
        max_infected_at_once = num_infected;

    is_saturated = num_infected > SATURATION_THRESHOLD;

    if ((elapsed_hours % 10) == 0 || num_infected == 0) {
        writefln("%d\t%d\t%d\t%d(%f%%)", num_vulnerable, num_infected, num_immune, num_dead, (num_dead * 100.0 / NUM_PEOPLE));
    }

    // Draw UI graph to show this !!
    infection_history[elapsed_hours] = num_infected;

    return num_infected;
}

Location get_location()
{
    Location l;
    l.x = uniform(0, X_LIMIT);
    l.y = uniform(0, Y_LIMIT);

    return l;
}

void pick_waypoint(ref Person p)
{
    p.speed = uniform(0, PP_TOP_SPEED) + 1;
    p.stay = uniform(0, MAX_STAY);
    if (try_event(p.home_probability)) {
        p.waypoint = p.home;
    } else {
        int idx = uniform(0, NUM_POPULAR_PLACES);
        p.waypoint = popular_places[idx];
    }
}

bool try_event(float probability)
{
    assert(probability >= 0.0 && probability <= 1.0);
    int resolution = 100000;
    float rnum = uniform(0, resolution);
    rnum = rnum / cast(float) resolution;
    return rnum <= probability;
}

void progress_disease(ref Person p)
{
    if (p.status == DISEASE_STATUS.INFECTED) {
        p.infection_counter--;
        if (p.infection_counter < 1) {
            // now you either become immune or die :-(
            float fatality_rate = (is_saturated) ? NORMAL_FATALITY_RATE : SATURATED_FATALITY_RATE;
            if (try_event(fatality_rate)) {
                p.status = DISEASE_STATUS.DEAD;
            } else {
                p.status = DISEASE_STATUS.IMMUNE;
            }
        }
    }
}

bool try_infect(ref Person p, Person p2)
{
    if (p2.status != DISEASE_STATUS.INFECTED)
        return false;

    if (get_distance(p.loc.x, p.loc.y, p2.loc.x, p2.loc.y) > INFECTION_PROXIMITY)
        return false;

    if (p.status == DISEASE_STATUS.VULNERABLE) {
        if (try_event(INFECTION_PROBABILITY)) {
            p.status = DISEASE_STATUS.INFECTED;
            p.infection_counter = INFECTION_TIME;
            return true;
        }
    }

    return false;
}

bool is_alive(Person p)
{
    return p.status != DISEASE_STATUS.DEAD;
}

void move(ref Person p)
{
    if (p.speed < 0) {
        pick_waypoint(p);
    } else if (at_location(p, p.waypoint)) {
        p.stay--;
        if (p.stay <= 0) {
            pick_waypoint(p);
        }
    } else {
        move_toward(p, p.waypoint.x, p.waypoint.y);
    }
}

bool move_toward(ref Person p, float dest_x, float dest_y)
{

    float dx = dest_x - p.loc.x;
    float dy = dest_y - p.loc.y;
    float theta = atan2(dy, dx);

    float distance = sqrt((dx * dx) + (dy * dy));

    if (distance < p.speed) {
        p.loc.x = dest_x;
        p.loc.y = dest_y;

        return true;
    } else {
        p.loc.x += (p.speed * cos(theta));
        p.loc.y += (p.speed * sin(theta));
        return false;
    }
}

float get_distance(float src_x, float src_y, float dest_x, float dest_y)
{
    float dx = dest_x - src_x;
    float dy = dest_y - src_y;

    return sqrt((dx * dx) + (dy * dy));
}

bool at_location(Person p, Location waypoint)
{
    return (get_distance(waypoint.x, waypoint.y, p.loc.x, p.loc.y) <= proximity);
}
