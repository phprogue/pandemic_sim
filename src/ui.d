module ui;

import std.stdio;
import std.math;
import std.random;
import std.string;
import std.conv;
import pandemic;

import raylib;

enum int SCREEN_WIDTH = 1200;
enum int SCREEN_HEIGHT = 800;
enum int MAP_WIDTH = 800;
enum int MAP_HEIGHT = 800;

enum int PEOPLE_SIZE = 8;
Font font_anonymous;

void init_ui()
{
    SetTargetFPS(60);
    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Pandemic Simulation");
    SetExitKey(KeyboardKey.KEY_Q);

    font_anonymous = LoadFont("Anonymous.ttf");

    int elapsed_hours = 0;
    int num_infected;

    while (!WindowShouldClose()) {

        num_infected = run(elapsed_hours);

        if (elapsed_hours % 2) {
            update_ui(elapsed_hours);
        }

        elapsed_hours++;
        if (elapsed_hours >= SIM_HOURS) {
            elapsed_hours -= 1;
            break;
        }

        if (num_infected == 0)
            break;
    }

    writefln("Peak Infections - %d", max_infected_at_once);

    while (!WindowShouldClose()) {
        // Simulation over
        // Do nothing - just enjoy the image
        update_ui(elapsed_hours);
    }

    UnloadFont(font_anonymous);
    CloseWindow();
}

void update_ui(int elapsed_hours)
{
    BeginDrawing();

    ClearBackground(Color( 235, 235, 235, 255 ));

    // draw screen divider
    DrawLine(MAP_WIDTH + PEOPLE_SIZE, 0, MAP_WIDTH + PEOPLE_SIZE, MAP_HEIGHT, Color(0, 0, 0, 255));

    foreach(pp; popular_places) {
        int[2] xy;
        xy = get_map_coords(pp.x, pp.y);
        // Popular Places = black outline
        DrawRectangleLines(xy[0], xy[1], PEOPLE_SIZE + 3, PEOPLE_SIZE + 3, Color(0, 0, 0, 255));
    }

    foreach (p; people) {
        int[2] xy;
        xy = get_map_coords(p.loc.x, p.loc.y);
        Color c = Color(100, 100, 100, 255);

        if (p.status == DISEASE_STATUS.INFECTED) {
            // Red
            c = Color(150, 0, 0, 255);
        } else if (p.status == DISEASE_STATUS.IMMUNE) {
            // Blue
            c = Color(0, 0, 150, 255);
        } else if (p.status == DISEASE_STATUS.DEAD) {
            // Black
            c = Color(0, 0, 0, 255);
        }
        DrawRectangle(xy[0], xy[1], PEOPLE_SIZE, PEOPLE_SIZE, c);
    }

    // draw infection_history
    draw_infection_history(elapsed_hours);

    draw_max_line();

    drawDeathStats();

    EndDrawing();
}

void draw_infection_history(int elapsed_hours)
{
    float h_mod = (cast(float) SCREEN_WIDTH - cast(float) MAP_WIDTH) / cast(float) SIM_HOURS;
    for (int e = 0; e < elapsed_hours; e++) {
        int start = MAP_WIDTH + PEOPLE_SIZE;
        int x = start + cast(int) (e * h_mod);
        Color c = Color(0, 0, 0, 255);
        if (infection_history[e] > SATURATION_THRESHOLD)
            c = Color(255, 0, 0, 255);
        DrawRectangle(x, MAP_HEIGHT - infection_history[e], 1, infection_history[e], c);
    }
}

void draw_max_line()
{
    const char* t = toStringz(to!string(max_infected_at_once) ~ " Max Infected at Once");
    Vector2 vec = {MAP_WIDTH + PEOPLE_SIZE + 20, MAP_HEIGHT - max_infected_at_once - 20};
    DrawTextEx(font_anonymous, t, vec, 16, 0, Color(0, 0, 0, 255));

    //DrawText(t, MAP_WIDTH + PEOPLE_SIZE + 20, MAP_HEIGHT - max_infected_at_once - 20, 14, Color(0, 0, 0, 255));
    DrawLine(MAP_WIDTH + PEOPLE_SIZE, MAP_HEIGHT - max_infected_at_once, SCREEN_WIDTH, MAP_HEIGHT - max_infected_at_once, Color(0, 0, 0, 255));
}

void drawDeathStats()
{
    const char* t = toStringz("Total Dead: " ~ to!string(num_dead) ~ " (" ~ to!string(num_dead * 100.0 / NUM_PEOPLE) ~ " %)");
    Vector2 vec = {MAP_WIDTH + 20, 20};
    DrawTextEx(font_anonymous, t, vec, 16, 0, Color(0, 0, 0, 255));
}

int[2] get_map_coords(float x, float y)
{
    float mw = cast(float) MAP_WIDTH;
    float xl = cast(float) X_LIMIT;
    float mod = (mw / xl);
    int new_x = cast(int) (x * mod);
    int new_y = cast(int) (y * mod);

    int[2] xy;
    xy[0] = new_x;
    xy[1] = new_y;
    return xy;
}
