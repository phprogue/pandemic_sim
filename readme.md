# Pandemic Simulator

Inspired by a COVID-19 simulator written in C++.

Graphical simulator showing 1000 "subjects" moving around - some visiting set locations and others practicing social distancing. They turn colors when infected and then when they either heal and become immune or they die.

Running totals of infected and dead are graphed on the right.

